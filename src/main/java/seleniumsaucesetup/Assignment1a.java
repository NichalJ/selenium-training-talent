package seleniumsaucesetup;

// 1. Imports for Selenium driver and Chrome driver

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

// Defines the class
public class Opdracht1a {

    // Main method
    public static void main(String[] args) {

        // 2. Initiates variable WebDriver and makes a new instance for the Driver     
            WebDriver driver = new ChromeDriver();
            //or 
            // Webdriver driver = null;
            // driver = new ChromeDriver();

        
        // 3. Open the browser with URL: https://www.saucedemo.com/
            driver.get("https://google.com/");
            System.out.println("Browser is open, what's next?");

        // 4. Fill in username ‘standard_user’ and password ‘secret_sauce’

        
        // 5. Press login
        
        
        // 6. close the browser
            //driver.close();
    
    }
    
}
