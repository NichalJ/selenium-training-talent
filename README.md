# Selenium Training

# Hoi toppers!

Dit is de Selenium Training die je krijgt tijdens de training testautomatisering.

Mocht er iets niet werken, just let us know. Vragen stellen is part of the job als je een tester bent. 

Have fun! :) 

# Benodigdheden

- IntelliJ Community Edition -> https://www.jetbrains.com/idea/download/
- Java 17 of 18
- Google Chrome

# Importeren

Hoe importeer je het project naar IntelliJ?

- Klik op Clone
- Klik op het icoontje onder 'Clone with HTTPS'
- In IntelliJ ga je naar File -> New -> Project from Version Control
- Plak de URL in het venster
- Klik op Clone
- Het feestje kan beginnen!

# Opdrachten

**Wat gaan we testen?**

Open in je browser de website https://www.saucedemo.com/ en bekijk de website.

_Opdracht 1A (klassikaal)_

1. Importeer de Selenium WebDriver en de ChromeDriver.
2. Initieer WebDriver als de variabele `driver` met de waarde null.
3. Geef binnen de main method het pad aan waar de ChromeDriver te vinden is.
4. Maak een nieuwe instance aan voor de ChromeDriver.
5. Open de browser met de URL: https://www.saucedemo.com/
6. Voer de username `standard_user` en wachtwoord `secret_sauce` in
7. Druk op login.
8. Sluit de browser.

**Opdracht 1B (zelfstandig)**

9. Open de browser met de URL: https://www.saucedemo.com/ en login met de standaard gebruiker.
10. Controleer of de titel “Swag Labs” juist wordt weergegeven.
11. Navigeer naar de pagina van ‘Sauce Labs Backpack’.
12. Controleer of de prijs correct is.
13. Voeg het item toe aan je winkelmandje.
14. Controleer of het item is toegevoegd aan je winkelmandje.
15. Sluit de browser.

**Opdracht 2 (zelfstandig)**

1. Open de browser met de URL: https://www.saucedemo.com/ en login met de probleem gebruiker.
2. Controleer of de titel “Swag Labs” juist wordt weergegeven.
3. Voeg een item toe aan je winkelmandje op de home pagina en verwijder deze weer.
4. Probeer op een andere manier een item uit je winkelmandje te verwijderen.
5. Sluit browser.

**Opdracht 3 (zelfstandig)**

Doel: zorg dat opdracht 1 en 2 achter elkaar worden uitgevoerd.

Eisen:
1. Browser wordt maar 1 keer opgestart en afgesloten.
2. Er is geen onnodige inputs en code

# Cheatsheet

WAJOOOO er is een cheatsheet! 
Check 'm in het bestand `CHEATSHEET_OMG.docx`

# Good to know

- Het kan zijn dat je de juiste versie van Java moet selecteren
- Het kan heel goed zijn dat je geen flauw idee hebt wat je moet doen

# Boodschap

Deze readme wordt vast nog wel een keer aangevuld, groetjes